import logo from "./logo.svg";
import "./App.css";
import Dataprovider from "./components/Dataprovider";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Register from "./components/Register";
import Login from "./components/Login";
import Dashboard from "./components/Dashboard";
import Form from "./components/Form";
import Table from "./components/Table";

function App() {
  return (
    <Dataprovider>
      <div className="App">
        <Router>
          <Routes>
            <Route exact path="/" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/dashboard" element={<Dashboard />} />
            <Route path="/form" element={<Form />} />
            <Route path="/table" element={<Table />} />
          </Routes>
        </Router>
      </div>
    </Dataprovider>
  );
}

export default App;
