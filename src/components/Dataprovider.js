import React, { createContext, useState } from "react";

export const DataContext = createContext();

function Dataprovider(props) {
  const [data, setData] = useState([]);
  const [userData, setUserData] = useState({
    name: "",
    qualBe: false,
    qualMtech: false,
    gender: "",
    country: "",
  });
  const [status, setStatus] = useState(false);
  const [index, setIndex] = useState("");
  return (
    <div>
      <DataContext.Provider
        value={[
          data,
          setData,
          userData,
          setUserData,
          status,
          setStatus,
          index,
          setIndex,
        ]}
      >
        {props.children}
      </DataContext.Provider>
    </div>
  );
}

export default Dataprovider;
