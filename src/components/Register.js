import React from "react";

import { useNavigate } from "react-router-dom";

function Register() {
  let navigate = useNavigate();

  const register = () => {
    navigate("/login");
  };
  return (
    <div>
      Register
      <button onClick={register}>Register</button>
    </div>
  );
}

export default Register;
